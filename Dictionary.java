import java.util.ArrayList;
/**
 * Lop Dictionary chua mang cac Word{word_target, word_explain}
 * ClassName: Dictionary
 * @author Nguyen Tuan Linh
 * @author Ha Minh Tuan
 * @version 1.8
 *
 */
public class Dictionary{
    public ArrayList<Word> arWords= new ArrayList<Word>();

    /**
     * Contructor
     */
    public Dictionary(){}
    /**
     * Them Word vao Dictionary
     */
    public void addWordToDictionary(Word word){
        arWords.add(word);
    }

}

