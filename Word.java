/**
 * Dinh nghia Word gom co tu goc va tu giai nghia
 * ClassName: Word
 * @author Nguyen Tuan Linh
 * @author Ha Minh Tuan
 * @version 1.8
 *
 */
public class Word{
    public String word_target, word_explain;
    /**
     * Constructor
     */
    public Word(){}
    /**
     * Constructor
     */
    public Word(String word_target, String word_explain){
        this.word_target = word_target;
        this.word_explain = word_explain;
    }

}