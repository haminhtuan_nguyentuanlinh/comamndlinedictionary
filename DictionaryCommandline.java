/**
 * ClassName: DictionaryCommandline
 * @author Nguyen Tuan Linh
 * @author Ha Minh Tuan
 * @version 1.8
 *
 */
import java.util.Scanner;


public class DictionaryCommandline{
    /**
     * Hien thi toan bo tu trong Dictionary
     */
    Dictionary dict;
    DictionaryManagement management;

    public DictionaryCommandline(Dictionary dict, DictionaryManagement management)
    {
        this.dict = dict;
        this.management = management;
    }

    public void showAllWords(){
        System.out.println(dict.arWords.size());
        int size = dict.arWords.size();
        System.out.printf("\n%-8s    | %-20s\t\t\t| Vietnamese\n\n","No","English");
        for(int i = 0; i < size; i++)
        {
            System.out.printf("%-8d    | %-20s\t\t\t| %s\n",i+1, dict.arWords.get(i).word_target, dict.arWords.get(i).word_explain);
        }

    }
    /**
     * v1: commandline so khai
     * goi ham chen insertFromCommandline()
     * goi ham hien thi showAllWords()
     */
    public void dictionaryBasic(){
        management.insertFromFile();
        management.showAllWords();
    }

    /**
     * v2: cai tien lan 1
     * goi ham chen insertFromFile()
     * goi ham hien thi showAllWords()
     * goi ham tra tu dictionaryLookup()
     */
    public void dictionaryAdvanced(){
        management.insertFromFile();
        management.showAllWords();
        management.dictionaryLookup();
    }

    public void dictionarySearcher(){
        System.out.println("Nhap tu muon search");
        Scanner sc = new Scanner(System.in);
        String enWord = sc.nextLine();

        System.out.printf("Serching result for \"%s\"...\n",enWord);

        for(int i =0 ;i< dict.arWords.size();i++)
        {
            boolean ok = false;
            for(int j = 0; j < enWord.length(); j++)
            {
                String temp = enWord.substring(0, j + 1);
                if (dict.arWords.get(i).word_target.contains(temp)) 
                {
                    ok = true;
                    break;
                }
            }
            if (ok) System.out.printf("%-8d    | %-20s\t\t\t| %s\n",i+1, dict.arWords.get(i).word_target, dict.arWords.get(i).word_explain);
        }
    }
}