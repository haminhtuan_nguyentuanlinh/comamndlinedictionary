import java.util.*;
public class Main
{
    public static void main(String[] args) {
        boolean isRuning = true;
        Main cmd = new Main();
        Dictionary dict = new Dictionary();
        DictionaryManagement management = new  DictionaryManagement(dict);
        DictionaryCommandline DictCmd = new DictionaryCommandline(dict, management);
        
        while (isRuning)
        {
            System.out.println("*************E_V Dictionary V2*************");
            System.out.println("");
            System.out.println("VUI LONG LUA CHON YEU CAU CUA BAN");
            System.out.println("0: Thoat ung dung");
            System.out.println("1: Them tu vao tu dien bang file");
            System.out.println("2: Hien thi danh sach tu");
            System.out.println("3: Tim kiem tu");
            System.out.println("4: Xoa tu");
            System.out.println("5: Xuat");
            System.out.println("6: Them tu bang Commandline");
            System.out.println("7: Tra tu");

            Scanner reader = new Scanner(System.in);
            char c = reader.next().charAt(0);

            switch (c) {
                case '0':
                    System.out.println("Dang thoat ung dung");
                    isRuning = false;
                    break;
                case '1':
                    dict = management.insertFromFile();
                    break;
                case '2':
                
                DictCmd.showAllWords();
                    break;
                case '3':
                    management.dictionaryLookup();
                    break;
                case '4':
                    management.deleteFromComandline();
                    break;
                case '5':
                    management.dictionaryExportToFile();
                    break;
                case '6':
                    management.insertFromCommandline();
                    break;
                case '7':
                    DictCmd.dictionarySearcher();
                    break;
                default:
                System.out.println("Nhap lai:");
                break;
            }
        }
    }
}